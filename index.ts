import crypto from 'node:crypto';

export enum CurrencyEnum {
    USD = 'USD',
    UAH = 'UAH'
}

export class Transaction {
    id: string;
    amount: number;
    currency: CurrencyEnum;

    constructor(amount: number, currency: CurrencyEnum) {
        this.id = crypto.randomUUID();
        this.amount = amount;
        this.currency = currency;
    }
}

// Оголошення інтерфейсу ICard
interface ICard {
    transactions: Transaction[];
    addTransaction(transaction: Transaction): string;
    addTransaction(currency: CurrencyEnum, amount: number): string;
    getTransaction(id: string): Transaction | undefined;
    getBalance(currency: CurrencyEnum): number;
}

export class Card implements ICard {
    transactions: Transaction[];

    constructor() {
        this.transactions = [];
    }


    // Implementation signature
    addTransaction(arg1: CurrencyEnum | Transaction, arg2?: number): string {
        if (typeof arg1 === 'object') {
            // якщо переданий об'єкт Transaction
            const transaction = arg1 as Transaction;
            this.transactions.push(transaction);
            return transaction.id;
        } else if (typeof arg1 === 'string' && typeof arg2 === 'number') {
            // якщо передані валюта та сума
            const currency = arg1 as CurrencyEnum;
            const amount = arg2 as number;
            const transaction = new Transaction(amount, currency);
            this.transactions.push(transaction);
            return transaction.id;
        }
        throw new Error('Invalid arguments');
    }

    getTransaction(id: string) {
        return this.transactions.find(transaction => transaction.id === id);
    }

    getBalance(currency: CurrencyEnum): number {
        return this.transactions.reduce((total, transaction) => {
            if (transaction.currency === currency) {
                return total + transaction.amount;
            } else {
                return total;
            }
        }, 0);
    }

}

export class BonusCard implements ICard {
    transactions: Transaction[];

    constructor() {
        this.transactions = [];
    }

    addTransaction(arg1: CurrencyEnum | Transaction, arg2?: number): string {
        let transaction: Transaction;
        let amount: number;

        if (typeof arg1 === 'object') {
            transaction = arg1 as Transaction;
            amount = transaction.amount;
        } else if (typeof arg1 === 'string' && typeof arg2 === 'number') {
            const currency = arg1 as CurrencyEnum;
            amount = arg2 as number;
            transaction = new Transaction(amount, currency);
        } else {
            throw new Error('Invalid arguments');
        }

        const bonusAmount = amount * 0.1; // обчислюємо розмір бонусної транзакції (10%)
        const bonusTransaction = new Transaction(bonusAmount, transaction.currency); // створюємо бонусну транзакцію
        this.transactions.push(transaction, bonusTransaction); // додаємо основну та бонусну транзакції
        return transaction.id;
    }

    getTransaction(id: string): Transaction | undefined {
        return this.transactions.find(transaction => transaction.id === id);
    }

    getBalance(currency: CurrencyEnum): number {
        return this.transactions.reduce((total, transaction) => {
            if (transaction.currency === currency) {
                return total + transaction.amount;
            } else {
                return total;
            }
        }, 0);
    }
}

export class Pocket {
    private cards: Map<string, ICard>;

    constructor() {
        this.cards = new Map<string, ICard>();
    }

    // Метод AddCard додає картку за переданим іменем
    addCard(name: string, card: ICard): void {
        this.cards.set(name, card);
    }

    // Метод RemoveCard видаляє картку за переданим іменем
    removeCard(name: string): void {
        this.cards.delete(name);
    }

    // Метод GetCard повертає картку за переданим іменем
    getCard(name: string): ICard | undefined {
        return this.cards.get(name);
    }

    // Метод GetTotalAmount повертає загальну суму усіх транзакцій в усіх картках за вказаною валютою
    getTotalAmount(currency: CurrencyEnum): number {
        let totalAmount = 0;
        for (const card of this.cards.values()) {
            totalAmount += card.getBalance(currency);
        }
        return totalAmount;
    }
}


const pocket = new Pocket();

const card1: ICard = new BonusCard();
const card2: ICard = new BonusCard();
pocket.addCard("Card1", card1);
pocket.addCard("Card2", card2);

card1.addTransaction(CurrencyEnum.USD, 100);
card1.addTransaction(CurrencyEnum.USD, 400);

card2.addTransaction(CurrencyEnum.UAH, 200);
card2.addTransaction(CurrencyEnum.UAH, 700);

console.log(pocket.getTotalAmount(CurrencyEnum.USD)); 
console.log(pocket.getTotalAmount(CurrencyEnum.UAH)); 