// import testFunc from './index';

// test('function', () => {
//     expect(testFunc()).toBe(true)
// });

import { CurrencyEnum } from './index';
import { Transaction } from './index';
import { Card } from './index';
import { BonusCard } from './index';
import { Pocket } from './index';

const pocket = new Pocket();


test('addCard', () => {
    const card1 = new BonusCard();
    const card2 = new BonusCard();

    pocket.addCard('Card1', card1);
    pocket.addCard('Card2', card2);

    expect(pocket.getCard('Card1')).toBe(card1);
    expect(pocket.getCard('Card2')).toBe(card2);
});


test('removeCard method', () => {
    const card = new BonusCard();
    pocket.addCard('Card', card);

    pocket.removeCard('Card');

    expect(pocket.getCard('Card')).toBeUndefined();
});


test('getTotalAmount method', () => {
    const card1 = new BonusCard();
    const card2 = new BonusCard();

    card1.addTransaction(CurrencyEnum.USD, 100);
    card2.addTransaction(CurrencyEnum.UAH, 200);

    pocket.addCard('Card1', card1);
    pocket.addCard('Card2', card2);

    expect(pocket.getTotalAmount(CurrencyEnum.USD)).toBe(110);
    expect(pocket.getTotalAmount(CurrencyEnum.UAH)).toBe(220);
});